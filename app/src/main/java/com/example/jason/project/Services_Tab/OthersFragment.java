package com.example.jason.project.Services_Tab;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
//import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.jason.project.Adapter.CustomAdapter;
import com.example.jason.project.DetailServiceActivity;
import com.example.jason.project.R;
import com.example.jason.project.Service;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;

/**
 * A simple {@link androidx.fragment.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OthersFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OthersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OthersFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public OthersFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OthersFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OthersFragment newInstance(String param1, String param2) {
        OthersFragment fragment = new OthersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_others, container, false);

        ListView listView = view.findViewById(R.id.othersList);
        final ArrayList<Service> service = new ArrayList<>();

        String menu1 = getString(R.string.others_menu_1);
        String menu2 = getString(R.string.others_menu_2);
        String menu3 = getString(R.string.others_menu_3);
        String menu4 = getString(R.string.others_menu_4);
        String menu5 = getString(R.string.others_menu_5);

        String desc1 = getString(R.string.others_desc_1);
        String desc2 = getString(R.string.others_desc_2);
        String desc3 = getString(R.string.others_desc_3);
        String desc4 = getString(R.string.others_desc_4);
        String desc5 = getString(R.string.others_desc_5);

        Service service1 = new Service(R.drawable.others1, menu1, desc1, 70000);
        Service service2 = new Service(R.drawable.others2, menu2, desc2, 75000);
        Service service3 = new Service(R.drawable.others3, menu3, desc3, 90000);
        Service service4 = new Service(R.drawable.others4, menu4, desc4, 20000);
        Service service5 = new Service(R.drawable.others5, menu5, desc5, 25000);

        service.add(service1);
        service.add(service2);
        service.add(service3);
        service.add(service4);
        service.add(service5);

        CustomAdapter ca = new CustomAdapter(getActivity(), service);
        listView.setAdapter(ca);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Service s = service.get(i);
                int img = s.getServiceImage();
                String name = s.getServiceName();
                String desc = s.getServiceDesc();
                int price = s.getServicePrice();

                String price2 = Integer.toString(price);

//                Bundle bundle = new Bundle();
//                bundle.putString("NAME", name);
//                bundle.putString("DESC", desc);
//                bundle.putInt("PRICE", price);

                Intent intent = new Intent(view.getContext(), DetailServiceActivity.class);
//                intent.putExtras(bundle);
                intent.putExtra("IMAGE", img);
                intent.putExtra("NAME", name);
                intent.putExtra("DESC", desc);
                intent.putExtra("PRICE", price2);
                startActivity(intent);
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
