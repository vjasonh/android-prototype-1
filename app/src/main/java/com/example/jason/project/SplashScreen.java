package com.example.jason.project;

import android.content.Intent;
import android.os.Handler;
//import android.support.v7.app.ActionBar;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

public class SplashScreen extends AppCompatActivity {

    private static int screenDuration = 3000;
    private TextView versionNumber;

    protected AlphaAnimation fadeIn = new AlphaAnimation(0.0f, 1.0f);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //View decorView = getWindow().getDecorView();
        // Hide the status bar.
        //int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        //decorView.setSystemUiVisibility(uiOptions);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Remember that you should never show the action bar if the status bar is hidden, so hide that too if necessary.
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }, screenDuration);

        versionNumber = (TextView) findViewById(R.id.verNumberSplash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                versionNumber.setAnimation(fadeIn);
//                versionNumber.setText(R.string.app_version);
                versionNumber.setText("v" + BuildConfig.VERSION_NAME);
                fadeIn.setDuration(300);
            }
        }, 1500);
    }
}