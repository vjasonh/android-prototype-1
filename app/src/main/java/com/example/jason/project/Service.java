package com.example.jason.project;

public class Service {

    private String serviceName;
    private String serviceDesc;
    private int serviceImage;
    private int servicePrice;


    public Service(int serviceImage, String serviceName, String serviceDesc, int servicePrice) {
        this.serviceName = serviceName;
        this.serviceDesc = serviceDesc;
        this.serviceImage = serviceImage;
        this.servicePrice = servicePrice;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDesc() {
        return serviceDesc;
    }

    public void setServiceDesc(String serviceDesc) {
        this.serviceDesc = serviceDesc;
    }

    public int getServiceImage() {
        return serviceImage;
    }

    public void setServiceImage(int serviceImage) {
        this.serviceImage = serviceImage;
    }

    public int getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(int servicePrice) {
        this.servicePrice = servicePrice;
    }
}
