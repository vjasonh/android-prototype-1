package com.example.jason.project;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
//import android.support.annotation.NonNull;
//import android.support.design.widget.NavigationView;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.app.ActionBar;
//import android.support.v7.app.ActionBarDrawerToggle;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.example.jason.project.databinding.ActivityHomeBinding;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

public class HomeActivity extends AppCompatActivity {

    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

//    private DrawerLayout dl;
    private ActionBarDrawerToggle toggle;
//    private ViewFlipper imageSlider;
    private TextView showUsername;
    private TextView editProfile;
//    private TextView welcomeScreenMessage;
    private Calendar getTime = Calendar.getInstance();

    ActivityHomeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_home);

        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

//        dl = findViewById(R.id.homeDrawer);
        toggle = new ActionBarDrawerToggle(this, binding.homeDrawer, R.string.open, R.string.close);
        binding.homeDrawer.addDrawerListener(toggle);
        toggle.syncState();

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setTitle(R.string.home_title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

//        final NavigationView nview = (NavigationView) findViewById(R.id.nav_view);
        View headerView = binding.navView.getHeaderView(0);
        showUsername = headerView.findViewById(R.id.username);

        SharedPreferences spf = getSharedPreferences("username", Context.MODE_PRIVATE);

        showUsername.setText(getString(R.string.welcome_message) + ", " + spf.getString("name", ""));

//        editProfile = headerView.findViewById(R.id.edit_profile_shortcut);
//        editProfile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                binding.homeDrawer.closeDrawer(binding.navView);
//                Intent intent = new Intent(getApplicationContext(), ProfileSettings.class);
//                startActivity(intent);
//            }
//        });

        binding.navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener(){

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                int id = item.getItemId();

                if (id == R.id.items){
                    binding.homeDrawer.closeDrawer(binding.navView);
                    Intent intent = new Intent(getApplicationContext(), ServicesActivity.class);
                    startActivity(intent);
                } else if (id == R.id.about){
                    binding.homeDrawer.closeDrawer(binding.navView);
                    Intent intent = new Intent(getApplicationContext(), AboutActivity.class);
                    startActivity(intent);
                } else if (id == R.id.settings){
                    binding.homeDrawer.closeDrawer(binding.navView);
                    Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                    startActivity(intent);
                } else if (id == R.id.logout){
                    firebaseAuth.getInstance().signOut();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    Toast.makeText(getBaseContext(), R.string.logout_message, Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

//        int currentHour = getTime.get(Calendar.HOUR); //to get current hour in 12 hours format
        int currentHour = getTime.get(Calendar.HOUR_OF_DAY); //to get current hour in 24 hours format
        String greetings = "";

        if (currentHour < 11){
            greetings = getString(R.string.morning);
        } else if (currentHour >= 11 && currentHour < 15){
            greetings = getString(R.string.afternoon);
        } else if (currentHour >= 15 && currentHour < 18){
            greetings = getString(R.string.evening);
        } else if (currentHour >= 18 && currentHour <= 24){
            greetings = getString(R.string.night);
        }

//        welcomeScreenMessage = findViewById(R.id.welcome_screen_message);
        binding.welcomeScreenMessage.setText(greetings + " " + spf.getString("name", ""));

        int images[] = {R.drawable.service1, R.drawable.service2, R.drawable.service3, R.drawable.service4, R.drawable.service5};

//        imageSlider = findViewById(R.id.imageSlider);

        for (int i = 0; i <images.length; i++){
            viewImage(images[i]);
        }
    }

    public void viewImage(int img){
        ImageView slider = new ImageView(this);
        slider.setBackgroundResource(img);

        binding.imageSlider.addView(slider);
        binding.imageSlider.setFlipInterval(5000);
        binding.imageSlider.setAutoStart(true);

        //setting sliding animation
        binding.imageSlider.setInAnimation(this, android.R.anim.fade_in);
        binding.imageSlider.setOutAnimation(this, android.R.anim.fade_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    boolean buttonPress = false;

    @Override
    public void onBackPressed() {
        if (buttonPress) {
            super.onBackPressed();
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            return;
        }

        this.buttonPress = true;
        Toast.makeText(getBaseContext(), R.string.exit_message, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                buttonPress = false;
            }
        }, 2000);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LanguageManager.onAttach(newBase));
    }
}
