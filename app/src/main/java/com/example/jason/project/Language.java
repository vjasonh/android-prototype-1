package com.example.jason.project;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

public class Language extends Application {
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LanguageManager.onAttach(base, "en"));
    }
}
