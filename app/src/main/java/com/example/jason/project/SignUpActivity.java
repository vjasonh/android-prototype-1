package com.example.jason.project;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
//import android.support.design.widget.TextInputLayout;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jason.project.databinding.ActivitySignUpBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class SignUpActivity extends AppCompatActivity {

//    EditText nameField;
//    EditText passwordField;
//    TextInputLayout passwordFieldLayout;
//    private CardView signupButton;
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private SharedPreferences spf;
    private String toastName;
    private String getTypedPass, getTypedPass2, confirmPass, confirmPass2;
    private int state = 0;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    ActivitySignUpBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_sign_up);

        binding = ActivitySignUpBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

//        nameField = findViewById(R.id.nameField2);
//        passwordField = findViewById(R.id.passwordField2);
//        signupButton = findViewById(R.id.signupButton);
//        passwordFieldLayout = findViewById(R.id.passwordFieldContainer2);

        binding.nameField2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!charSequence.toString().matches(emailPattern)){
                    binding.nameField2.setError("Enter a valid email!");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        binding.passwordField2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                binding.confirmPasswordField.setText("");
                getTypedPass = charSequence.toString();
                if (!Pattern.matches("^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$", getTypedPass)){
                    binding.passwordField2.setError("Password must be alphanumeric!");
                    if(!getTypedPass.equals(binding.confirmPasswordField.getEditableText())){
                        binding.confirmPasswordField.setError("Password doesn't match!");
                    } else if(getTypedPass.equals(binding.confirmPasswordField.getEditableText())){
                        binding.confirmPasswordField.setError(null);
                    }
                } else if(getTypedPass.length() < 8){
                    binding.passwordField2.setError("Password must contain at least 8 characters!");
                    if(!getTypedPass.equals(binding.confirmPasswordField.getText())){
                        binding.confirmPasswordField.setError("Password doesn't match!");
                    } else if(getTypedPass.equals(binding.confirmPasswordField.getEditableText())){
                        binding.confirmPasswordField.setError(null);
                    }
                } else if(!binding.passwordField2.getEditableText().equals(binding.confirmPasswordField.getEditableText())){
                    binding.confirmPasswordField.setError("Password doesn't match!");
                } else {
                    binding.confirmPasswordField.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                getTypedPass2 = editable.toString();
            }
        });

        binding.confirmPasswordField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                confirmPass = charSequence.toString();
                if(!confirmPass.equals(getTypedPass)){
                    binding.confirmPasswordField.setError("Password doesn't match!");
                } else {
                    binding.confirmPasswordField.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                confirmPass2 = editable.toString();
            }
        });

        SpannableString toLogin = new SpannableString(binding.backToLogin.getText());
        toLogin.setSpan(new UnderlineSpan(), 0, toLogin.length(), 0);
        binding.backToLogin.setText(toLogin);

        binding.backToLogin.setTextColor(Color.BLUE);

        binding.backToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gotoLogin = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(gotoLogin);
            }
        });

        binding.signupButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                final String checkName = binding.nameField2.getText().toString();
                String checkPass = binding.passwordField2.getText().toString();
                String checkPass2 = binding.confirmPasswordField.toString();
//                Dialog setDialog = null;

                if (checkName.isEmpty() && checkPass.isEmpty() && checkPass2.isEmpty()){
                    Toast.makeText(getBaseContext(), "Please input your data!", Toast.LENGTH_SHORT).show();
                } else if (checkName.equals("")){
//                    setDialog = errorMessage("Name must be filled!");
//                    setDialog.show();
//                    Toast.makeText(getBaseContext(), R.string.warn1, Toast.LENGTH_SHORT).show();
                    binding.nameField2.setError("Enter your username!");
                    binding.nameField2.requestFocus();
                } else if (checkName.length() <= 5){
//                    setDialog = errorMessage("Name must be more than 5 characters!");
//                    setDialog.show();
//                    Toast.makeText(getBaseContext(), R.string.warn2, Toast.LENGTH_SHORT).show();
                    binding.nameField2.setError("Username must be more than 5 characters!");
                    binding.nameField2.requestFocus();
                } else if (checkPass.equals("")){
//                    setDialog = errorMessage("Password must be filled!");
//                    setDialog.show();
//                    Toast.makeText(getBaseContext(), R.string.warn3, Toast.LENGTH_SHORT).show();
                    binding.passwordField2.setError("Enter your password!");
                    binding.passwordField2.requestFocus();
                } else if (!Pattern.matches("^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$", checkPass)){
//                    Toast.makeText(getBaseContext(), R.string.warn4, Toast.LENGTH_SHORT).show();
                    binding.passwordField2.setError("Password must be alphanumeric!");
                    binding.passwordField2.requestFocus();
                } else if (checkPass.length() < 8){
//                    Toast.makeText(getBaseContext(), R.string.warn5, Toast.LENGTH_SHORT).show();
                    binding.passwordField2.setError("Password must contain at least 8 characters!");
                    binding.passwordField2.requestFocus();
                } else if(checkPass2.isEmpty()){
                    binding.confirmPasswordField.setError("confirm your password!");
                    binding.confirmPasswordField.requestFocus();
                } else if(!getTypedPass2.equals(confirmPass2)){
                    binding.confirmPasswordField.setError("Password doesn't match!");
                } else if(!(checkName.isEmpty() && checkPass.isEmpty() && checkPass2.isEmpty())){
                    saveName(checkName);
                    //setDialog = successMessage();

//                    final ProgressDialog loading = ProgressDialog.show(SignUpActivity.this, getString(R.string.load1), getString(R.string.load2), true, true);

                    firebaseAuth.createUserWithEmailAndPassword(checkName, checkPass).addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                                finish();
                                Toast.makeText(getBaseContext(), R.string.signupSuccess, Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getBaseContext(), "Email has already registered!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

//                    spf = getSharedPreferences("username", Context.MODE_PRIVATE);
//                    toastName = getString(R.string.login_message) + " " + spf.getString("name", "") + "!";
//                    Toast.makeText(getBaseContext(), toastName, Toast.LENGTH_LONG).show();
//                    finish();
                } else {
                    Toast.makeText(getBaseContext(), "An error occured!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        /* SHOW/HIDE PASSWORD TOGGLE */
//        passwordFieldLayout.setPasswordVisibilityToggleEnabled(false);
//        passwordField.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if(charSequence.toString().equals("")){
//                    passwordFieldLayout.setPasswordVisibilityToggleEnabled(false);
//                } else {
//                    passwordFieldLayout.setPasswordVisibilityToggleEnabled(true);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
    }

    public void saveName(String checkName){
        spf = getSharedPreferences("username", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = spf.edit();
        editor.putString("name", checkName);
        editor.apply();
    }

//    public Dialog errorMessage(String message){
//        final AlertDialog.Builder errorAlert = new AlertDialog.Builder(this);
//        errorAlert.setTitle("Error Message")
//                .setMessage(message)
//                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int i) {
//                        dialog.dismiss();
//                    }
//                });
//        return errorAlert.create();
//    }

//    public Dialog successMessage(){
//        final AlertDialog.Builder errorAlert = new AlertDialog.Builder(this);
//        errorAlert.setTitle("Success Message")
//                .setMessage("Login Successfully!")
//                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int i) {
//                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
//                        startActivity(intent);
//                        spf = getSharedPreferences("username", Context.MODE_PRIVATE);
//                        toastName = "Welcome back, " + spf.getString("name", "") + "!";
//                        Toast.makeText(getApplicationContext(), toastName, Toast.LENGTH_LONG).show();
//                        finish();
//                        dialog.dismiss();
//                    }
//                });
//        return errorAlert.create();
//    }

    boolean buttonPress = false;

    @Override
    public void onBackPressed() {
        if (buttonPress) {
            super.onBackPressed();
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            return;
        }
        this.buttonPress = true;
        Toast.makeText(getBaseContext(), R.string.exit_message, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                buttonPress = false;
            }
        }, 2000);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LanguageManager.onAttach(newBase));
    }
}
