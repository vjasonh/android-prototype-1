package com.example.jason.project.Adapter;

//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.jason.project.Services_Tab.ClothingFragment;
import com.example.jason.project.Services_Tab.OthersFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class TabAdapter extends FragmentStatePagerAdapter {

    int numOfTab;

    public TabAdapter(FragmentManager fm, int numOfTab){
        super(fm);
        this.numOfTab = numOfTab;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                ClothingFragment cf = new ClothingFragment();
                return cf;

            case 1:
                OthersFragment of = new OthersFragment();
                return of;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTab;
    }
}
