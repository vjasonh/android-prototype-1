package com.example.jason.project;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
//import android.support.annotation.Nullable;
//import android.support.v7.app.ActionBar;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jason.project.databinding.ActivityDetailServiceBinding;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class DetailServiceActivity extends AppCompatActivity {

//    TextView userName;
//    TextView desc;
//    TextView price;
//    TextView warning;
//    EditText qty;
//    ImageView picture;
//    private CardView order;

    ActivityDetailServiceBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_detail_service);

        binding = ActivityDetailServiceBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.order_detail_title);
        actionBar.setDisplayHomeAsUpEnabled(true);

//        userName = findViewById(R.id.nameView);
//        desc = findViewById(R.id.descView);
//        price = findViewById(R.id.priceView);
//        qty = findViewById(R.id.qtyField);
//        order = findViewById(R.id.orderButton);
//        picture = findViewById(R.id.pictView);
//        warning = findViewById(R.id.warning);

//        Bundle bundle = getIntent().getExtras();

        Intent i = getIntent();

        int gambar = i.getIntExtra("IMAGE", 0);
        String nama = i.getStringExtra("NAME");
        String deskripsi = i.getStringExtra("DESC");
        final String harga = i.getStringExtra("PRICE");

        binding.pictView.setImageResource(gambar);
        binding.nameView.setText(nama);
        binding.descView.setText(deskripsi);
        binding.priceView.setText("Rp. " + harga);

        binding.orderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog setDialog = null;

                int qtyTemp = Integer.parseInt(String.valueOf(binding.qtyField.getText()));

                if (qtyTemp < 1){
                    setDialog = errorMessage(getString(R.string.warning2));
                } else {
                    setDialog = confirmationMessage();
                }
                setDialog.show();
            }
        });

        binding.orderButton.setEnabled(false);
        binding.orderButton.setAlpha((float) 0.4);
        binding.orderButton.setCardBackgroundColor(Color.parseColor("#7B7B7B"));
        binding.qtyField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().equals("")){
                    binding.warning.setText(R.string.warning);
                    binding.orderButton.setEnabled(false);
                    binding.orderButton.setCardBackgroundColor(Color.parseColor("#7B7B7B"));
                    binding.orderButton.setAlpha((float) 0.4);
                } else {
                    binding.warning.setText("");

                    int qtyTemp = Integer.parseInt(String.valueOf(binding.qtyField.getText()));

                    int priceInt = Integer.parseInt(String.valueOf(harga));
                    int total = qtyTemp * priceInt;
                    if(qtyTemp >= 1){
                        binding.warning.setText("Subtotal: Rp." + total);
                    }

                    if(qtyTemp > 20){
                        binding.qtyField.setText("20");
                        int count = binding.qtyField.length();
                        binding.qtyField.setSelection(count);
                        Toast.makeText(getBaseContext(), getString(R.string.warning3), Toast.LENGTH_LONG).show();
                    }
                    binding.orderButton.setCardBackgroundColor(Color.parseColor("#FFFF4444"));
                    binding.orderButton.setAlpha(1);
                    binding.orderButton.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public Dialog errorMessage(String message){
        final AlertDialog.Builder errorAlert = new AlertDialog.Builder(this);
        errorAlert.setTitle(R.string.error)
                .setMessage(message)
                .setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });
        return errorAlert.create();
    }

    public Dialog successMessage(){
        final AlertDialog.Builder errorAlert = new AlertDialog.Builder(this);
        errorAlert.setTitle(R.string.success)
                .setMessage(R.string.success_message)
                .setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        Intent intent = new Intent(getApplicationContext(), ServicesActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        dialog.dismiss();
                        finish();
                    }
                });
        return errorAlert.create();
    }

    public Dialog confirmationMessage(){
        final AlertDialog.Builder errorAlert = new AlertDialog.Builder(this);

        errorAlert.setTitle(R.string.confirm)
                .setMessage(R.string.confirm_message)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
//                        final ProgressDialog loading = ProgressDialog.show(DetailServiceActivity.this, getString(R.string.load1), getString(R.string.load2), true, true);
                        dialog.dismiss();

                        Dialog setWarn = successMessage();
                        setWarn.show();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });
        return errorAlert.create();
    }

    public boolean onOptionsItemSelected(MenuItem item){

        switch ( item.getItemId() ) {
            case android.R.id.home:
                super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);

//        Intent myIntent = new Intent(getApplicationContext(), ServicesActivity.class);
//        startActivityForResult(myIntent, 0);
//        return true;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LanguageManager.onAttach(newBase));
    }
}
