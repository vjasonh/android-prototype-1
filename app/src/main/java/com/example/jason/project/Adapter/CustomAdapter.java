package com.example.jason.project.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jason.project.R;
import com.example.jason.project.Service;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<Service> {

    private Context context;
    private ArrayList<Service>list;

    public CustomAdapter(Context context, ArrayList<Service>list){
        super(context, 0, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public View getView(int position, View ConvertView, ViewGroup parent){
        View listItem = ConvertView;

        if (listItem == null) listItem = LayoutInflater.from(context).inflate(R.layout.custom_layout, parent, false);

        Service currService = list.get(position);

        ImageView serviceImage = listItem.findViewById(R.id.serviceImage);
        serviceImage.setImageResource(currService.getServiceImage());

        TextView serviceName = listItem.findViewById(R.id.serviceName);
        serviceName.setText(currService.getServiceName());

        TextView serviceDesc = listItem.findViewById(R.id.serviceDesc);
        serviceDesc.setText(currService.getServiceDesc());

        TextView servicePrice = listItem.findViewById(R.id.servicePrice);
        servicePrice.setText("Rp. " + Integer.toString(currService.getServicePrice()));

        return listItem;
    }
}
