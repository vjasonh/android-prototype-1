package com.example.jason.project;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
//import android.support.design.widget.TextInputLayout;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jason.project.databinding.ActivityLoginBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Locale;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

public class LoginActivity extends AppCompatActivity {

//    EditText nameField;
//    EditText passwordField;
//    TextInputLayout passwordFieldLayout;
//    private CardView loginButton;
    FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private SharedPreferences spf;
    private String toastName;

    ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_login);

        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

//        nameField = findViewById(R.id.nameField);
//        passwordField = findViewById(R.id.passwordField);
//        loginButton = findViewById(R.id.loginButton);
//        passwordFieldLayout = findViewById(R.id.passwordFieldContainer);

        SpannableString signup = new SpannableString(binding.signupLabel.getText());

        ClickableSpan signupClickable = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                Intent gotoSignUp = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(gotoSignUp);
                finish();
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.BLUE);
            }
        };

        signup.setSpan(signupClickable, signup.length()-5, signup.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.signupLabel.setText(signup);
        binding.signupLabel.setMovementMethod(LinkMovementMethod.getInstance());

        binding.loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String checkName = binding.nameField.getText().toString();
                String checkPass = binding.passwordField.getText().toString();

//                Dialog setDialog = null;

                if (checkName.isEmpty() && checkPass.isEmpty()){
                    Toast.makeText(getBaseContext(), "Fields are empty!", Toast.LENGTH_SHORT).show();
                } else if(checkName.equals("")){
//                    setDialog = errorMessage("Name must be filled!");
//                    setDialog.show();
//                    Toast.makeText(getBaseContext(), R.string.warn1, Toast.LENGTH_SHORT).show();
                    binding.nameField.setError("Enter your username!");
                    binding.nameField.requestFocus();
                } else if (checkPass.equals("")){
//                    setDialog = errorMessage("Password must be filled!");
//                    setDialog.show();
//                    Toast.makeText(getBaseContext(), R.string.warn3, Toast.LENGTH_SHORT).show();
                    binding.passwordField.setError("Enter your password!");
                    binding.passwordField.requestFocus();
                } else if(!(checkName.isEmpty() && checkPass.isEmpty())){

                    saveName(checkName);
                    //setDialog = successMessage();

                    firebaseAuth.signInWithEmailAndPassword(checkName, checkPass).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()){
                                Toast.makeText(getBaseContext(), R.string.invalidLogin, Toast.LENGTH_SHORT).show();
                            } else {
                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();
                                Toast.makeText(getBaseContext(), R.string.validLogin, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

//                    final ProgressDialog loading = ProgressDialog.show(LoginActivity.this, getString(R.string.load1), getString(R.string.load2), true, true);
//
//                    spf = getSharedPreferences("username", Context.MODE_PRIVATE);
//                    toastName = getString(R.string.login_message) + " " + spf.getString("name", "") + "!";
//                    Toast.makeText(getBaseContext(), toastName, Toast.LENGTH_LONG).show();
//                    finish();
                } else {
                    Toast.makeText(getBaseContext(), "An error occured!", Toast.LENGTH_SHORT).show();
                }
            }
        });

         /* SHOW/HIDE PASSWORD TOGGLE */
        binding.passwordFieldContainer.setPasswordVisibilityToggleEnabled(false);
        binding.passwordField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.toString().equals("")){
                    binding.passwordFieldContainer.setPasswordVisibilityToggleEnabled(false);
                } else {
                    binding.passwordFieldContainer.setPasswordVisibilityToggleEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void saveName(String checkName){
        spf = getSharedPreferences("username", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = spf.edit();
        editor.putString("name", checkName);
        editor.apply();
    }

//    public Dialog errorMessage(String message){
//        final AlertDialog.Builder errorAlert = new AlertDialog.Builder(this);
//        errorAlert.setTitle("Error Message")
//                .setMessage(message)
//                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int i) {
//                        dialog.dismiss();
//                    }
//                });
//        return errorAlert.create();
//    }

//    public Dialog successMessage(){
//        final AlertDialog.Builder errorAlert = new AlertDialog.Builder(this);
//        errorAlert.setTitle("Success Message")
//                .setMessage("Login Successfully!")
//                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int i) {
//                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
//                        startActivity(intent);
//                        spf = getSharedPreferences("username", Context.MODE_PRIVATE);
//                        toastName = "Welcome back, " + spf.getString("name", "") + "!";
//                        Toast.makeText(getApplicationContext(), toastName, Toast.LENGTH_LONG).show();
//                        finish();
//                        dialog.dismiss();
//                    }
//                });
//        return errorAlert.create();
//    }

    boolean buttonPress = false;

    @Override
    public void onBackPressed() {
        if (buttonPress) {
            super.onBackPressed();
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            return;
        }
        this.buttonPress = true;
        Toast.makeText(getBaseContext(), R.string.exit_message, Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                buttonPress = false;
            }
        }, 2000);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LanguageManager.onAttach(newBase));
    }
}
