package com.example.jason.project;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
//import android.support.annotation.NonNull;
//import android.support.design.widget.NavigationView;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.app.ActionBar;
//import android.support.v7.app.ActionBarDrawerToggle;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jason.project.databinding.ActivityAboutBinding;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

public class AboutActivity extends AppCompatActivity {

    FirebaseAuth firebaseAuth;

//    private DrawerLayout dl;
    private ActionBarDrawerToggle toggle;
    private TextView showUsername;
    private TextView editProfile;

    ActivityAboutBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_about);

        binding = ActivityAboutBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

//        dl = findViewById(R.id.aboutDrawer);
        toggle = new ActionBarDrawerToggle(this, binding.aboutDrawer, R.string.open, R.string.close);
        binding.aboutDrawer.addDrawerListener(toggle);
        toggle.syncState();

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setTitle(R.string.about_title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

//        final NavigationView nview = (NavigationView) findViewById(R.id.nav_view2);
        View headerView = binding.navView2.getHeaderView(0);
        showUsername = headerView.findViewById(R.id.username);

        SharedPreferences result = getSharedPreferences("username", Context.MODE_PRIVATE);

        showUsername.setText(getString(R.string.welcome_message) + ", " + result.getString("name", ""));

//        editProfile = headerView.findViewById(R.id.edit_profile_shortcut);
//        editProfile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                binding.aboutDrawer.closeDrawer(binding.navView2);
//                Intent intent = new Intent(getApplicationContext(), ProfileSettings.class);
//                startActivity(intent);
//            }
//        });

        binding.navView2.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener(){

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                int id = item.getItemId();

                if (id == R.id.home){
                    binding.aboutDrawer.closeDrawer(binding.navView2);
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                } else if (id == R.id.items){
                    binding.aboutDrawer.closeDrawer(binding.navView2);
                    Intent intent = new Intent(getApplicationContext(), ServicesActivity.class);
                    startActivity(intent);
                } else if (id == R.id.settings){
                    binding.aboutDrawer.closeDrawer(binding.navView2);
                    Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                    startActivity(intent);
                } else if (id == R.id.logout){
                    firebaseAuth.getInstance().signOut();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    Toast.makeText(getApplicationContext(), R.string.logout_message, Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
            super.onBackPressed();
            Intent goHome = new Intent(getApplicationContext(), HomeActivity.class);
            goHome.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(goHome);
            finish();
            overridePendingTransition(0, 0);
            return;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LanguageManager.onAttach(newBase));
    }
}
