package com.example.jason.project;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
//import android.support.v14.preference.SwitchPreference;
//import android.support.v7.app.ActionBar;
import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.preference.ListPreference;
//import android.support.v7.preference.Preference;
//import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.Locale;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SwitchPreference;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
            actionBar.setTitle(R.string.settings_title);
            actionBar.setDisplayHomeAsUpEnabled(true);

        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    public boolean onOptionsItemSelected(MenuItem item){

        switch ( item.getItemId() ) {
            case android.R.id.home:
                super.onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LanguageManager.onAttach(newBase));
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {

        protected ProgressDialog loading;
        Handler handler = new Handler();
//        Preference profilePreference = (Preference) findPreference("profile_preference");

        @Override
        public void onCreatePreferences(final Bundle bundle, final String s) {
            setPreferencesFromResource(R.xml.settings, s);

            Preference pref = (Preference) findPreference("versioning");
            pref.setSummary("v" + BuildConfig.VERSION_NAME);

            final ListPreference listPreference = (ListPreference) findPreference("settings1");
            if(listPreference.getValue() == null) {
                // to ensure we don't get a null value
                // set first value by default
                listPreference.setValueIndex(0);
            }

            listPreference.setSummary(listPreference.getEntry().toString());
            listPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValues) {
                    listPreference.setValue(newValues.toString());
                    preference.setSummary(listPreference.getEntry());

                    //Changing app language through app settings
                    if (preference.getSummary().toString().equals("English") && Locale.getDefault().getLanguage().equals("in")) {
                        LanguageManager.setLocale(getContext(), "en");

                        loading = new ProgressDialog(getContext());
                        loading.setMessage(getString(R.string.change_lang));
                        loading.setIndeterminate(true);
                        loading.setCancelable(false);
                        loading.show();

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    Thread.sleep(5000);
                                }catch(InterruptedException ex){
                                    ex.printStackTrace();
                                }
                                loading.dismiss();
                                Intent intent = new Intent(getContext(), HomeActivity.class);
                                startActivity(intent);
                            }
                        }).start();

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (Locale.getDefault().getLanguage().equals("en")){
                                    Toast.makeText(getContext(), "Language updated!", Toast.LENGTH_SHORT).show();
                                } else if(Locale.getDefault().getLanguage().equals("in")){
                                    Toast.makeText(getContext(), "Bahasa diperbaharui!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, 5500);
                    } else if (preference.getSummary().toString().equals("English") && Locale.getDefault().getLanguage().equals("en")){
                        Toast.makeText(getContext(), "Language already set to English!", Toast.LENGTH_SHORT).show();
                    }

                    if (preference.getSummary().toString().equals("Bahasa Indonesia") && Locale.getDefault().getLanguage().equals("en")) {
                        LanguageManager.setLocale(getContext(), "in");

                        loading = new ProgressDialog(getContext());
                        loading.setMessage(getString(R.string.change_lang));
                        loading.setIndeterminate(true);
                        loading.setCancelable(false);
                        loading.show();

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    Thread.sleep(5000);
                                }catch(InterruptedException ex){
                                    ex.printStackTrace();
                                }
                                loading.dismiss();
                                Intent intent = new Intent(getContext(), HomeActivity.class);
                                startActivity(intent);
                            }
                        }).start();

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (Locale.getDefault().getLanguage().equals("en")){
                                    Toast.makeText(getContext(), "Language updated!", Toast.LENGTH_SHORT).show();
                                } else if(Locale.getDefault().getLanguage().equals("in")){
                                    Toast.makeText(getContext(), "Bahasa diperbaharui!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, 5500);
                    } else if (preference.getSummary().toString().equals("Bahasa Indonesia") && Locale.getDefault().getLanguage().equals("in")){
                        Toast.makeText(getContext(), "Bahasa sudah di set ke Bahasa Indonesia!", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
            });

            SwitchPreference switchPreference = (SwitchPreference) findPreference("settings2");

            switchPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {

                    boolean isEnable = (boolean) newValue;

                    if (isEnable){
//                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                        Toast.makeText(getContext(), getString(R.string.dark_mode_on), Toast.LENGTH_SHORT).show();
                    } else {
//                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                        Toast.makeText(getContext(), getString(R.string.dark_mode_off), Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
            });
        }
    }
}
