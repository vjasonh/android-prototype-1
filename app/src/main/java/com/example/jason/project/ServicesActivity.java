package com.example.jason.project;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
//import android.support.annotation.NonNull;
//import android.support.design.widget.NavigationView;
//import android.support.design.widget.TabLayout;
//import android.support.v4.view.ViewPager;
//import android.support.v4.widget.DrawerLayout;
//import android.support.v7.app.ActionBar;
//import android.support.v7.app.ActionBarDrawerToggle;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jason.project.Adapter.TabAdapter;
import com.example.jason.project.Services_Tab.ClothingFragment;
import com.example.jason.project.Services_Tab.OthersFragment;
import com.example.jason.project.databinding.ActivityServicesBinding;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

public class ServicesActivity extends AppCompatActivity implements ClothingFragment.OnFragmentInteractionListener, OthersFragment.OnFragmentInteractionListener {

    FirebaseAuth firebaseAuth;

//    private DrawerLayout dl;
    private ActionBarDrawerToggle toggle;
    private TextView showUsername;
    private TextView editProfile;

    ActivityServicesBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_services);

        binding = ActivityServicesBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

//        dl = findViewById(R.id.servicesDrawer);
        toggle = new ActionBarDrawerToggle(this, binding.servicesDrawer, R.string.open, R.string.close);
        binding.servicesDrawer.addDrawerListener(toggle);
        toggle.syncState();

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setTitle(R.string.services_title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

//        final NavigationView nview = (NavigationView) findViewById(R.id.nav_view);
        View headerView = binding.navView.getHeaderView(0);
        showUsername = headerView.findViewById(R.id.username);

        SharedPreferences result = getSharedPreferences("username", Context.MODE_PRIVATE);

        showUsername.setText(getString(R.string.welcome_message) + ", " + result.getString("name", ""));

//        editProfile = headerView.findViewById(R.id.edit_profile_shortcut);
//        editProfile.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                binding.servicesDrawer.closeDrawer(binding.navView);
//                Intent intent = new Intent(getApplicationContext(), ProfileSettings.class);
//                startActivity(intent);
//            }
//        });

        binding.navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener(){

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                int id = item.getItemId();

                if (id == R.id.home){
                    binding.servicesDrawer.closeDrawer(binding.navView);
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                } else if (id == R.id.about){
                    binding.servicesDrawer.closeDrawer(binding.navView);
                    Intent intent = new Intent(getApplicationContext(), AboutActivity.class);
                    startActivity(intent);
                } else if (id == R.id.settings){
                    binding.servicesDrawer.closeDrawer(binding.navView);
                    Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                    startActivity(intent);
                } else if (id == R.id.logout){
                    firebaseAuth.getInstance().signOut();
                    Intent intent = new Intent(ServicesActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    Toast.makeText(getBaseContext(), R.string.logout_message, Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.clothing));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.others));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = findViewById(R.id.pager);
        final TabAdapter adapter = new TabAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(toggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent goHome = new Intent(getApplicationContext(), HomeActivity.class);
        goHome.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(goHome);
        finish();
        overridePendingTransition(0, 0);
        return;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LanguageManager.onAttach(newBase));
    }
}
